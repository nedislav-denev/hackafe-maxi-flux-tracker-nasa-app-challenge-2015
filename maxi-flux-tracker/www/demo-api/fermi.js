window.demoData = window.demoData || {};

window.demoData.fermi = [
   {
      "name":"PMN J0017-0512",
      "coordinates":"(RA = 4.39900, Dec = -5.21200)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ0017-0512_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ0017-0512_604800.png"
   },
   {
      "name":"4C +01.02",
      "coordinates":"(RA = 17.1620, Dec = 1.58300)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/4C+01.02_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/4C+01.02_604800.png"
   },
   {
      "name":"4C 31.03",
      "coordinates":"(RA = 18.2100, Dec = 32.1380)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/4C31.03_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/4C31.03_604800.png"
   },
   {
      "name":"PKS 0130-17",
      "coordinates":"(RA = 23.1810, Dec = -16.9130)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0130-17_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0130-17_604800.png"
   },
   {
      "name":"0208-512",
      "coordinates":"(RA = 32.6930, Dec = -51.0170)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/0208-512_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/0208-512_604800.png"
   },
   {
      "name":"CGRaBS J0211+1051",
      "coordinates":"(RA = 32.8050, Dec = 10.8600)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/CGRaBSJ0211+1051_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/CGRaBSJ0211+1051_604800.png"
   },
   {
      "name":"S3 0218+35",
      "coordinates":"(RA = 35.2730, Dec = 35.9370)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S30218+35_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S30218+35_604800.png"
   },
   {
      "name":"3C 66A",
      "coordinates":"(RA = 35.6650, Dec = 43.0350)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/3C66A_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/3C66A_604800.png"
   },
   {
      "name":"PKS 0235-618",
      "coordinates":"(RA = 39.2220, Dec = -61.6040)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0235-618_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0235-618_604800.png"
   },
   {
      "name":"4C +28.07",
      "coordinates":"(RA = 39.4680, Dec = 28.8020)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/4C+28.07_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/4C+28.07_604800.png"
   },
   {
      "name":"0235+164",
      "coordinates":"(RA = 39.6620, Dec = 16.6160)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/0235+164_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/0235+164_604800.png"
   },
   {
      "name":"LSI +61 303",
      "coordinates":"(RA = 40.1310, Dec = 61.2290)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/LSI+61303_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/LSI+61303_604800.png"
   },
   {
      "name":"PKS 0244-470",
      "coordinates":"(RA = 41.5000, Dec = -46.8550)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0244-470_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0244-470_604800.png"
   },
   {
      "name":"PKS 0250-225",
      "coordinates":"(RA = 43.2000, Dec = -22.3240)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0250-225_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0250-225_604800.png"
   },
   {
      "name":"PKS 0301-243",
      "coordinates":"(RA = 45.8600, Dec = -24.1200)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0301-243_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0301-243_604800.png"
   },
   {
      "name":"NGC 1275",
      "coordinates":"(RA = 49.9510, Dec = 41.5120)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/NGC1275_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/NGC1275_604800.png"
   },
   {
      "name":"1H 0323+342",
      "coordinates":"(RA = 51.1720, Dec = 34.1790)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1H0323+342_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1H0323+342_604800.png"
   },
   {
      "name":"PKS 0336-01",
      "coordinates":"(RA = 54.8790, Dec = -1.77700)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0336-01_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0336-01_604800.png"
   },
   {
      "name":"4C +50.11",
      "coordinates":"(RA = 59.8740, Dec = 50.9640)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/4C+50.11_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/4C+50.11_604800.png"
   },
   {
      "name":"PKS 0402-362",
      "coordinates":"(RA = 60.9740, Dec = -36.0840)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0402-362_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0402-362_604800.png"
   },
   {
      "name":"PKS 0426-380",
      "coordinates":"(RA = 67.1680, Dec = -37.9390)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0426-380_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0426-380_604800.png"
   },
   {
      "name":"NRAO 190",
      "coordinates":"(RA = 70.6610, Dec = -0.295000)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/NRAO190_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/NRAO190_604800.png"
   },
   {
      "name":"PKS 0454-234",
      "coordinates":"(RA = 74.2630, Dec = -23.4140)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0454-234_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0454-234_604800.png"
   },
   {
      "name":"PKS 0458-02",
      "coordinates":"(RA = 75.3030, Dec = -1.98700)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0458-02_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0458-02_604800.png"
   },
   {
      "name":"PKS 0502+049",
      "coordinates":"(RA = 76.3470, Dec = 4.99500)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0502+049_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0502+049_604800.png"
   },
   {
      "name":"PKS 0507+17",
      "coordinates":"(RA = 77.5100, Dec = 18.0120)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0507+17_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0507+17_604800.png"
   },
   {
      "name":"VER 0521+211",
      "coordinates":"(RA = 80.4420, Dec = 21.2140)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/VER0521+211_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/VER0521+211_604800.png"
   },
   {
      "name":"PKS 0521-36",
      "coordinates":"(RA = 80.7420, Dec = -36.4590)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0521-36_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0521-36_604800.png"
   },
   {
      "name":"PKS 0528+134",
      "coordinates":"(RA = 82.7350, Dec = 13.5320)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0528+134_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0528+134_604800.png"
   },
   {
      "name":"CRATES J0531-4827",
      "coordinates":"(RA = 82.9940, Dec = -48.4600)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/CRATESJ0531-4827_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/CRATESJ0531-4827_604800.png"
   },
   {
      "name":"OG 050",
      "coordinates":"(RA = 83.1620, Dec = 7.54500)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/OG050_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/OG050_604800.png"
   },
   {
      "name":"Crab Pulsar",
      "coordinates":"(RA = 83.6330, Dec = 22.0140)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/CrabPulsar_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/CrabPulsar_604800.png"
   },
   {
      "name":"PKS 0537-441",
      "coordinates":"(RA = 84.7100, Dec = -44.0860)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0537-441_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0537-441_604800.png"
   },
   {
      "name":"B2 0619+33",
      "coordinates":"(RA = 95.7180, Dec = 33.4360)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B20619+33_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B20619+33_604800.png"
   },
   {
      "name":"MG2 J071354+1934",
      "coordinates":"(RA = 108.482, Dec = 19.5830)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/MG2J071354+1934_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/MG2J071354+1934_604800.png"
   },
   {
      "name":"PKS 0716+714",
      "coordinates":"(RA = 110.430, Dec = 71.3500)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0716+714_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0716+714_604800.png"
   },
   {
      "name":"4C 14.23",
      "coordinates":"(RA = 111.320, Dec = 14.4200)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/4C14.23_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/4C14.23_604800.png"
   },
   {
      "name":"PKS 0727-11",
      "coordinates":"(RA = 112.580, Dec = -11.6870)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0727-11_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0727-11_604800.png"
   },
   {
      "name":"PKS 0736+01",
      "coordinates":"(RA = 114.825, Dec = 1.61800)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0736+01_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0736+01_604800.png"
   },
   {
      "name":"BZU J0742+5444",
      "coordinates":"(RA = 115.666, Dec = 54.7400)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/BZUJ0742+5444_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/BZUJ0742+5444_604800.png"
   },
   {
      "name":"PKS 0805-07",
      "coordinates":"(RA = 122.065, Dec = -7.85300)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0805-07_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0805-07_604800.png"
   },
   {
      "name":"0827+243",
      "coordinates":"(RA = 127.490, Dec = 24.2200)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/0827+243_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/0827+243_604800.png"
   },
   {
      "name":"S5 0836+71",
      "coordinates":"(RA = 130.352, Dec = 70.8950)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S50836+71_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S50836+71_604800.png"
   },
   {
      "name":"OJ 287",
      "coordinates":"(RA = 133.704, Dec = 20.1090)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/OJ287_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/OJ287_604800.png"
   },
   {
      "name":"3EG J0903-3531",
      "coordinates":"(RA = 136.202, Dec = -35.2560)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/3EGJ0903-3531_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/3EGJ0903-3531_604800.png"
   },
   {
      "name":"PKS B0906+015",
      "coordinates":"(RA = 137.292, Dec = 1.36000)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKSB0906+015_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKSB0906+015_604800.png"
   },
   {
      "name":"0FGL J0910.2-5044",
      "coordinates":"(RA = 137.568, Dec = -50.7430)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/0FGLJ0910.2-5044_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/0FGLJ0910.2-5044_604800.png"
   },
   {
      "name":"PKS 0920-39",
      "coordinates":"(RA = 140.693, Dec = -39.9930)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0920-39_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS0920-39_604800.png"
   },
   {
      "name":"PMN J0948+0022",
      "coordinates":"(RA = 147.239, Dec = 0.374000)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ0948+0022_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ0948+0022_604800.png"
   },
   {
      "name":"S4 0954+65",
      "coordinates":"(RA = 149.697, Dec = 65.5650)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S40954+65_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S40954+65_604800.png"
   },
   {
      "name":"S4 1030+61",
      "coordinates":"(RA = 158.464, Dec = 60.8520)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S41030+61_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S41030+61_604800.png"
   },
   {
      "name":"PMN J1038-5311",
      "coordinates":"(RA = 159.669, Dec = -53.1950)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ1038-5311_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ1038-5311_604800.png"
   },
   {
      "name":"S5 1044+71",
      "coordinates":"(RA = 162.115, Dec = 71.7270)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S51044+71_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S51044+71_604800.png"
   },
   {
      "name":"J1057-6027",
      "coordinates":"(RA = 164.308, Dec = -60.4580)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/J1057-6027_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/J1057-6027_604800.png"
   },
   {
      "name":"4C +01.28",
      "coordinates":"(RA = 164.623, Dec = 1.56600)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/4C+01.28_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/4C+01.28_604800.png"
   },
   {
      "name":"Mrk 421",
      "coordinates":"(RA = 166.114, Dec = 38.2090)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/Mrk421_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/Mrk421_604800.png"
   },
   {
      "name":"PMN J1123-6417",
      "coordinates":"(RA = 170.828, Dec = -64.2970)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ1123-6417_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ1123-6417_604800.png"
   },
   {
      "name":"B2 1144+40",
      "coordinates":"(RA = 176.743, Dec = 39.9760)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B21144+40_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B21144+40_604800.png"
   },
   {
      "name":"1150+497",
      "coordinates":"(RA = 178.352, Dec = 49.5190)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1150+497_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1150+497_604800.png"
   },
   {
      "name":"Ton 599",
      "coordinates":"(RA = 179.883, Dec = 29.2460)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/Ton599_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/Ton599_604800.png"
   },
   {
      "name":"W Comae",
      "coordinates":"(RA = 185.382, Dec = 28.2330)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/WComae_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/WComae_604800.png"
   },
   {
      "name":"PKS B1222+216",
      "coordinates":"(RA = 186.227, Dec = 21.3800)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKSB1222+216_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKSB1222+216_604800.png"
   },
   {
      "name":"3C 273",
      "coordinates":"(RA = 187.278, Dec = 2.05200)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/3C273_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/3C273_604800.png"
   },
   {
      "name":"J123939+044409",
      "coordinates":"(RA = 189.900, Dec = 4.70000)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/J123939+044409_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/J123939+044409_604800.png"
   },
   {
      "name":"PKS 1244-255",
      "coordinates":"(RA = 191.695, Dec = -25.7970)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1244-255_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1244-255_604800.png"
   },
   {
      "name":"3C 279",
      "coordinates":"(RA = 194.047, Dec = -5.78900)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/3C279_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/3C279_604800.png"
   },
   {
      "name":"OP 313",
      "coordinates":"(RA = 197.619, Dec = 32.3450)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/OP313_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/OP313_604800.png"
   },
   {
      "name":"GB6 B1310+4844",
      "coordinates":"(RA = 198.181, Dec = 48.4750)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/GB6B1310+4844_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/GB6B1310+4844_604800.png"
   },
   {
      "name":"PKS 1329-049",
      "coordinates":"(RA = 203.019, Dec = -5.16200)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1329-049_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1329-049_604800.png"
   },
   {
      "name":"B3 1343+451",
      "coordinates":"(RA = 206.388, Dec = 44.8830)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B31343+451_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B31343+451_604800.png"
   },
   {
      "name":"PKS 1346-112",
      "coordinates":"(RA = 207.381, Dec = -11.5480)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1346-112_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1346-112_604800.png"
   },
   {
      "name":"1406-076",
      "coordinates":"(RA = 212.235, Dec = -7.87400)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1406-076_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1406-076_604800.png"
   },
   {
      "name":"PKS 1424-41",
      "coordinates":"(RA = 216.985, Dec = -42.1050)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1424-41_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1424-41_604800.png"
   },
   {
      "name":"H 1426+428",
      "coordinates":"(RA = 217.136, Dec = 42.6720)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/H1426+428_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/H1426+428_604800.png"
   },
   {
      "name":"PKS 1451-375",
      "coordinates":"(RA = 223.614, Dec = -37.7930)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1451-375_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1451-375_604800.png"
   },
   {
      "name":"PKS 1454-354",
      "coordinates":"(RA = 224.361, Dec = -35.6530)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1454-354_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1454-354_604800.png"
   },
   {
      "name":"PKS 1502+106",
      "coordinates":"(RA = 226.104, Dec = 10.4940)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1502+106_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1502+106_604800.png"
   },
   {
      "name":"B2 1504+37",
      "coordinates":"(RA = 226.540, Dec = 37.5140)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B21504+37_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B21504+37_604800.png"
   },
   {
      "name":"J1512-3221",
      "coordinates":"(RA = 228.040, Dec = -32.3600)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/J1512-3221_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/J1512-3221_604800.png"
   },
   {
      "name":"1510-089",
      "coordinates":"(RA = 228.170, Dec = -8.83000)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1510-089_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1510-089_604800.png"
   },
   {
      "name":"B2 1520+31",
      "coordinates":"(RA = 230.542, Dec = 31.7370)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B21520+31_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B21520+31_604800.png"
   },
   {
      "name":"TXS 1530-131",
      "coordinates":"(RA = 233.160, Dec = -13.3500)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/TXS1530-131_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/TXS1530-131_604800.png"
   },
   {
      "name":"PKS 1622-253",
      "coordinates":"(RA = 246.445, Dec = -25.4610)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1622-253_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1622-253_604800.png"
   },
   {
      "name":"PKS B 1622-297",
      "coordinates":"(RA = 246.525, Dec = -29.8570)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKSB1622-297_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKSB1622-297_604800.png"
   },
   {
      "name":"PMN J1626-2426",
      "coordinates":"(RA = 246.750, Dec = -24.4450)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ1626-2426_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ1626-2426_604800.png"
   },
   {
      "name":"1633+382",
      "coordinates":"(RA = 248.815, Dec = 38.1350)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1633+382_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1633+382_604800.png"
   },
   {
      "name":"0FGL J1641.4+3939",
      "coordinates":"(RA = 250.355, Dec = 39.6660)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/0FGLJ1641.4+3939_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/0FGLJ1641.4+3939_604800.png"
   },
   {
      "name":"Mrk 501",
      "coordinates":"(RA = 253.468, Dec = 39.7600)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/Mrk501_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/Mrk501_604800.png"
   },
   {
      "name":"GB6 J1700+6830",
      "coordinates":"(RA = 255.039, Dec = 68.5020)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/GB6J1700+6830_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/GB6J1700+6830_604800.png"
   },
   {
      "name":"B3 1708+433",
      "coordinates":"(RA = 257.421, Dec = 43.3120)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B31708+433_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B31708+433_604800.png"
   },
   {
      "name":"Fermi J1717-5156",
      "coordinates":"(RA = 259.394, Dec = -51.9250)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/FermiJ1717-5156_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/FermiJ1717-5156_604800.png"
   },
   {
      "name":"1730-130",
      "coordinates":"(RA = 263.261, Dec = -13.0800)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1730-130_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1730-130_604800.png"
   },
   {
      "name":"S4 1749+70",
      "coordinates":"(RA = 267.137, Dec = 70.0970)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S41749+70_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S41749+70_604800.png"
   },
   {
      "name":"S5 1803+78",
      "coordinates":"(RA = 270.190, Dec = 78.4680)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S51803+78_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/S51803+78_604800.png"
   },
   {
      "name":"PKS 1824-582",
      "coordinates":"(RA = 277.302, Dec = -58.2320)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1824-582_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1824-582_604800.png"
   },
   {
      "name":"PKS 1830-211",
      "coordinates":"(RA = 278.416, Dec = -21.0610)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1830-211_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS1830-211_604800.png"
   },
   {
      "name":"CGRaBS J1848+3219",
      "coordinates":"(RA = 282.092, Dec = 32.3170)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/CGRaBSJ1848+3219_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/CGRaBSJ1848+3219_604800.png"
   },
   {
      "name":"CGRaBS J1849+6705",
      "coordinates":"(RA = 282.317, Dec = 67.0950)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/CGRaBSJ1849+6705_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/CGRaBSJ1849+6705_604800.png"
   },
   {
      "name":"PKS B1908-201",
      "coordinates":"(RA = 287.790, Dec = -20.1150)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKSB1908-201_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKSB1908-201_604800.png"
   },
   {
      "name":"PMN J1913-3630",
      "coordinates":"(RA = 288.337, Dec = -36.5050)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ1913-3630_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ1913-3630_604800.png"
   },
   {
      "name":"1ES 1959+650",
      "coordinates":"(RA = 300.000, Dec = 65.1490)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1ES1959+650_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1ES1959+650_604800.png"
   },
   {
      "name":"Fermi J2007-2518",
      "coordinates":"(RA = 302.371, Dec = -25.4110)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/FermiJ2007-2518_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/FermiJ2007-2518_604800.png"
   },
   {
      "name":"PKS 2023-07",
      "coordinates":"(RA = 306.419, Dec = -7.59800)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2023-07_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2023-07_604800.png"
   },
   {
      "name":"PKS 2123-463",
      "coordinates":"(RA = 321.628, Dec = -46.0970)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2123-463_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2123-463_604800.png"
   },
   {
      "name":"PKS 2136-642",
      "coordinates":"(RA = 325.003, Dec = -64.0260)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2136-642_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2136-642_604800.png"
   },
   {
      "name":"OX 169",
      "coordinates":"(RA = 325.898, Dec = 17.7300)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/OX169_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/OX169_604800.png"
   },
   {
      "name":"PKS 2142-75",
      "coordinates":"(RA = 326.803, Dec = -75.6040)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2142-75_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2142-75_604800.png"
   },
   {
      "name":"PKS 2149-306",
      "coordinates":"(RA = 327.981, Dec = -30.4650)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2149-306_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2149-306_604800.png"
   },
   {
      "name":"PKS 2155-304",
      "coordinates":"(RA = 329.717, Dec = -30.2260)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2155-304_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2155-304_604800.png"
   },
   {
      "name":"NRAO 676",
      "coordinates":"(RA = 330.431, Dec = 50.8160)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/NRAO676_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/NRAO676_604800.png"
   },
   {
      "name":"PKS 2155-83",
      "coordinates":"(RA = 330.583, Dec = -83.6370)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2155-83_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2155-83_604800.png"
   },
   {
      "name":"BL Lac",
      "coordinates":"(RA = 330.680, Dec = 42.2780)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/BLLac_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/BLLac_604800.png"
   },
   {
      "name":"3C 446",
      "coordinates":"(RA = 336.447, Dec = -4.95000)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/3C446_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/3C446_604800.png"
   },
   {
      "name":"CTA 102",
      "coordinates":"(RA = 338.152, Dec = 11.7310)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/CTA102_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/CTA102_604800.png"
   },
   {
      "name":"PKS 2233-148",
      "coordinates":"(RA = 339.142, Dec = -14.5560)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2233-148_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2233-148_604800.png"
   },
   {
      "name":"TXS 2241+406",
      "coordinates":"(RA = 341.053, Dec = 40.9540)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/TXS2241+406_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/TXS2241+406_604800.png"
   },
   {
      "name":"PMN J2250-2806",
      "coordinates":"(RA = 342.685, Dec = -28.1100)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ2250-2806_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ2250-2806_604800.png"
   },
   {
      "name":"3C 454.3",
      "coordinates":"(RA = 343.491, Dec = 16.1480)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/3C454.3_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/3C454.3_604800.png"
   },
   {
      "name":"PKS 2255-282",
      "coordinates":"(RA = 344.525, Dec = -27.9730)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2255-282_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2255-282_604800.png"
   },
   {
      "name":"B2 2308+34",
      "coordinates":"(RA = 347.772, Dec = 34.4200)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B22308+34_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/B22308+34_604800.png"
   },
   {
      "name":"PKS 2320-035",
      "coordinates":"(RA = 350.883, Dec = -3.28500)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2320-035_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2320-035_604800.png"
   },
   {
      "name":"1ES 2322-409",
      "coordinates":"(RA = 351.186, Dec = -40.6800)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1ES2322-409_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1ES2322-409_604800.png"
   },
   {
      "name":"PKS 2326-502",
      "coordinates":"(RA = 352.337, Dec = -49.9280)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2326-502_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PKS2326-502_604800.png"
   },
   {
      "name":"PMN J2345-1555",
      "coordinates":"(RA = 356.302, Dec = -15.9190)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ2345-1555_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/PMNJ2345-1555_604800.png"
   },
   {
      "name":"1ES 2344+514",
      "coordinates":"(RA = 356.770, Dec = 51.7050)",
      "dailylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1ES2344+514_86400.png",
      "weeklylC":"/FTP/glast/data/lat/catalogs/asp/current/lightcurves/1ES2344+514_604800.png"
   }
]