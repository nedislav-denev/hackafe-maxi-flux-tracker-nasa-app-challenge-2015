angular.module('starter.services', [])

.factory('Monitors', function($http, $q) {
  return {

    monitorsData: null,

    init: function () {
        var defer = $q.defer();

        $http.get('http://nasa.cnapsys.com/api/Values')
        .success(function (data) {
            defer.resolve(data);
        }).error(function (status, code) {
            defer.reject('Cannot fetch json file');
        });

        return defer.promise;
    },

    /***
     * Gets all monitor data or data for a specific monitor
     * @param {String} monitor
     */
    all: function(monitorName) {

        return monitorName ? this.monitorsData[monitorName] : this.monitorsData;
    },

    get: function(monitorName, dataId) {
        var result, length, searchAttribute;


        if (this.monitorsData[monitorName]) {

            searchAttribute = monitorName === 'fermi' ? 'name' : 'id';

            length = this.monitorsData[monitorName].length -1;

            for (var i = 0; i < length ; i++) {
                if (this.monitorsData[monitorName][i][searchAttribute] === dataId) {
                    result = this.monitorsData[monitorName][i];
                }
            }
        }

        return result;
    }
  };
});
