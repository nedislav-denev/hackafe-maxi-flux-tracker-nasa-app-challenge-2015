angular.module('starter.controllers', [])

.controller('MonitorsCtrl', function($scope, $rootScope, Monitors, $ionicLoading) {
    $ionicLoading.show({template: 'Loading'});

    $scope.bexrb = null;
    $scope.fermi = null;
    $scope.swift = null;

    Monitors.init().then(function(data) {
        $ionicLoading.hide();
        Monitors.monitorsData = data;
        Monitors.monitorsData.swift = Monitors.monitorsData.swift || window.demoData.swift;
        Monitors.monitorsData.fermi = Monitors.monitorsData.fermi || window.demoData.fermi;
        $scope.bexrb = data.bexrb.length ? true : false;
        $scope.fermi = data.fermi ? true : false;
        $scope.swift = data.swift ? true : false;
    });
})

.controller('DataCtrl', function($scope, $stateParams, Monitors, $ionicLoading) {

    $scope.monitorName = $stateParams.monitorName;

    if(Monitors.monitorsData) {
        $scope.monitors = Monitors.all($stateParams.monitorName);
    }else {

        $ionicLoading.show({template: 'Loading'});

        Monitors.init().then(function(data) {
            Monitors.monitorsData = data;
            Monitors.monitorsData.swift = Monitors.monitorsData.swift || window.demoData.swift;
            $ionicLoading.hide();
            $scope.monitors = Monitors.all($stateParams.monitorName);
        });
    }
})

.controller('DataDetailCtrl', function($scope, $stateParams, Monitors) {
    $scope.monitorName = $stateParams.monitorName;
    $scope.monitor = Monitors.get($stateParams.monitorName, $stateParams.dataId);

})
.controller('InfoCtrl', function($scope) {
  // $scope.settings = {
  //   enableFriends: true
  // };
});
